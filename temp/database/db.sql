-- to create a new database
CREATE DATABASE Ikyata;

-- to use database
use Ikyata;

-- creating a new table
CREATE TABLE events (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(50) NOT NULL,
  type VARCHAR(100) NOT NULL,
  description VARCHAR(15)
);

-- to show all tables
show tables;

-- to describe table
describe event;
